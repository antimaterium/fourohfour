import json 
import os
import fhirclient.models.patient as p
import fhirclient.models.bundle as b
import fhirclient.models.immunization as immu
import numpy
import pandas as pd
from sklearn import preprocessing
import sklearn.decomposition.factor_analysis as factor_analysis
import matplotlib.pyplot as pl



class PatientStruct:
    def __init__(self):
        self.firstname = []
        self.lastname = []
        self.birthdate = []
        self.deathdate = []
        self.location = []
        self.gender  = []
        self.race  = []
        self.ethnicity  = []

        self.bodyweight = []
        self.bodyheight = []
        self.bmi = []

        self.had_cancer = []


class TimeStruct:
    def __init__(self, a, b, c):
        self.year = a
        self.month = b
        self.day = c 

def timeComp(timeM, timeN):
    if(timeN.year > timeM.year):
        return True
    elif(timeN.year == timeM.year) and (timeN.month > timeM.month):
        return True
    elif(timeN.year == timeM.year) and (timeN.month > timeM.month) and (timeN.day > timeM.day):
        return True
    else:
        return False

def cumulativeget(maxtime, maxval, res):
    time = res.get("issued")
    newDate = TimeStruct(int(time[0:4]), int(time[5:7]), int(time[8:10]))
    if(timeComp(maxtime, newDate)):
            maxtime = newDate
            maxval = res.get("valueQuantity").get("value")
    return maxtime, maxval

patients = PatientStruct()

dirname = "./fhir"

for filename in os.listdir(dirname):
    with open(dirname+"/"+filename, 'r') as h:
        pjs = json.load(h)
        pj = json.dumps(pjs)
        d = json.loads(pj)
        patient = 0
        condition = 0
        immunization = 0

        has_immu = False

        maxDateWeight   = TimeStruct(0,0,0)
        maxDateHeight    = TimeStruct(0,0,0)
        maxDateBMI    = TimeStruct(0,0,0)

        newestBodyWeight  = -1
        newestBodyHeight  = -1
        newestBodyBMI = -1

        hadCancer = 0

      #  print(len(d.get("entry")))
        for i in range(len(d.get("entry"))):
            rtype = d.get("entry")[i].get("resource").get("resourceType")
            res  = d.get("entry")[i].get("resource")

            if( rtype == "Patient" ):
                patient = ((d.get("entry")[i]).get("resource"))
            if( rtype == "Condition" ):
                condition = ((d.get("entry")[i]).get("resource"))
            if( rtype == "Immunization" ):
                has_immu = True
                immunization = ((d.get("entry")[i]).get("resource"))
            if( rtype == "Observation" ):
                if(res.get("code").get("coding")[0].get("display") == "Body Weight"):
                    maxDateWeight, newestBodyWeight = cumulativeget(maxDateWeight, newestBodyWeight, res)
                if(res.get("code").get("coding")[0].get("display") == "Body Height"):
                    maxDateHeight, newestBodyHeight = cumulativeget(maxDateHeight, newestBodyHeight, res)
                if(res.get("code").get("coding")[0].get("display") == "Body Mass Index"):
                    maxDateBMI, newestBodyBMI = cumulativeget(maxDateBMI, newestBodyBMI, res)


            if( rtype == "Observation" ):
                if(res.get("code").get("coding")[0].get("display") == "Sexual Orientation"):
                    print(res.get("valueString").get("value"))
        
        patient = p.Patient(patient)
        if(has_immu):
            immunization = immu.Immunization(immunization)

        patients.firstname.append(patient.name[0].given[0])
        patients.lastname.append(patient.name[0].family)
        bd = TimeStruct(int(str(patient.birthDate.date)[0:4]),int(str(patient.birthDate.date)[5:7]),int(str(patient.birthDate.date)[8:10]))
        patients.birthdate.append(bd)
        patients.bodyweight.append(newestBodyWeight)
        patients.bodyheight.append(newestBodyHeight)
        patients.bmi.append(newestBodyBMI)

    h.close()
    

todeletes = []
for i in range(len(patients.firstname)):
   # print(patients.firstname[i], patients.lastname[i])
   # print(patients.bodyweight[i], patients.bodyheight[i])
   # print(patients.bmi[i], patients.bmi[i])
    if(patients.bodyweight[i] == -1)or(patients.bodyheight[i] == -1)or(patients.bmi == -1):
        todeletes.append(i)

for i in range(len(todeletes)):
    del patients.firstname[todeletes[i]-i]
    del patients.lastname[todeletes[i]-i]

    del patients.bodyweight[todeletes[i]-i]
    del patients.bodyheight[todeletes[i]-i]
    del patients.bmi[todeletes[i]-i]

    del patients.birthdate[todeletes[i]-i]

for i in range(len(patients.firstname)):
    asdf = 0
    #print(patients.firstname[i], patients.lastname[i])
    #print(patients.bodyweight[i], patients.bodyheight[i])
    #print(patients.bmi[i], patients.bmi[i])

#X = numpy.array([   [2.0, 4.0,8.0,16.0], 
#                    [2.0, 4.0,8.0,16.0],
#                    [2.0, 4.0,8.0,16.0],
#                    [16.0, 8.0,4.0,2.0]])
ages = []
for i in range(len(patients.birthdate)):
    ages.append(2018 - patients.birthdate[i].year)
X = numpy.array([
        patients.bodyweight,
        patients.bodyheight,
        patients.bmi,
        ages
        ])
assert( len(patients.bodyweight) == len(patients.bodyheight) == len(patients.bmi) == len(ages) )
print(ages)
y = numpy.rot90(X)
Xn = preprocessing.scale(y)
#print(Xn)

for i in range(1,4+1):
    temp = Xn
    facanalysis = factor_analysis.FactorAnalysis(n_components=i).fit(temp)
    #print(facanalysis.components_)
    print (pd.DataFrame(facanalysis.components_))


def bmigraph(X):
    pl.figure()
#pl.errorbar(times, means, yerr=sems, marker='x', linestyle = '', ecolor = (1, 0, 0), color = (0, 0, 0))
    xes = X[0,:]
    yes = X[1,:]

#draw BMI lines
#Underweight 16  18.5 
#Normal (healthy weight) 18.5    25 
#Overweight  25  30  
#Obese Class I (Moderately obese)    30  35  
#kg/m^2
    bmis = [[16, 18.5, 25, 30],["Underweight","Normal","Overweight","Obese"]]

    x = numpy.linspace(max(xes),  min(xes))
    y0 = 100*numpy.sqrt(x/float(bmis[0][0]))
    y1 = 100*numpy.sqrt(x/float(bmis[0][1]))
    y2 = 100*numpy.sqrt(x/float(bmis[0][2]))
    y3 = 100*numpy.sqrt(x/float(bmis[0][3]))


    pl.plot(x,y0,color='purple', label="Severely Underweight Line")
    pl.plot(x,y1,color='green', label="Underweight Line")
    pl.plot(x,y2,color='green',label="Overweight Line")
    pl.plot(x,y3,color='red',label="Obesity Line")
    pl.legend()

#pl.plot(xes, yes)

    pl.scatter(xes, yes, s= numpy.array(ages)*0.7)
    pl.xlabel("weight / kg")
    pl.ylabel("height / cm")
    pl.title("Heights and Weights in the FHIR dataset")
    pl.show()

